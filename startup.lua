local env = require("env")

local startup_apps = {
	"albert",
	"pasystray",
	"picom",
	"scream"
}

for i, app in ipairs(startup_apps) do
	env.awful.spawn.with_shell("! pgrep -u $USER " .. app .. "&& " .. app)
end
