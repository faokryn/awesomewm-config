local env = require("env")
local menubar = require("menubar")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
env.beautiful.init(env.awesome_dir .. "theme/theme.lua")

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with env.awful.layout.inc, order matters.
env.awful.layout.layouts = {
    env.awful.layout.suit.spiral.dwindle,
    env.awful.layout.suit.tile,
    env.awful.layout.suit.tile.left,
    env.awful.layout.suit.tile.bottom,
    env.awful.layout.suit.tile.top,
    env.awful.layout.suit.fair,
    env.awful.layout.suit.fair.horizontal,
    env.awful.layout.suit.floating,
    -- env.awful.layout.suit.spiral,
    -- env.awful.layout.suit.max,
    -- env.awful.layout.suit.max.fullscreen,
    -- env.awful.layout.suit.magnifier,
    -- env.awful.layout.suit.corner.nw,
    -- env.awful.layout.suit.corner.ne,
    -- env.awful.layout.suit.corner.sw,
    -- env.awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, env.awful.screen.focused()) end },
   { "manual", env.default_programs.terminal .. " -e man awesome" },
   { "edit config", env.default_programs.editor .. " " .. env.awesome_dir },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = env.awful.menu({ items = { { "awesome", myawesomemenu, env.beautiful.awesome_icon },
                                    { "open terminal", env.default_programs.terminal }
                                  }
                        })

mylauncher = env.awful.widget.launcher({ image = env.beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = env.default_programs.terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = env.awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
clock = env.wibox.widget.textclock("%a %F %R %Z", 30)

-- Create a wibox for each screen and add it
local taglist_buttons = env.gears.table.join(
                    env.awful.button({ }, 1, function(t) t:view_only() end),
                    env.awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    env.awful.button({ }, 3, env.awful.tag.viewtoggle),
                    env.awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    env.awful.button({ }, 4, function(t) env.awful.tag.viewnext(t.screen) end),
                    env.awful.button({ }, 5, function(t) env.awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = env.gears.table.join(
                     env.awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     env.awful.button({ }, 3, function()
                                              env.awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     env.awful.button({ }, 4, function ()
                                              env.awful.client.focus.byidx(1)
                                          end),
                     env.awful.button({ }, 5, function ()
                                              env.awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    local wallpaper = (function ()
        local w = env.beautiful.wallpaper or "";

        if type(w) == "table" then
            local n = 0;
            for _ in pairs(w) do n = n + 1 end
            return tostring(w[s.index % n + 1] or "")
        elseif type(w) == "string" then
            return w
        else
            return ""
        end
    end)()
    env.gears.wallpaper.maximized(wallpaper, s)
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

env.awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    env.awful.tag({ "1", "2", "3", "4", "5" }, s, env.awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = env.awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = env.awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(env.gears.table.join(
                           env.awful.button({ }, 1, function () env.awful.layout.inc( 1) end),
                           env.awful.button({ }, 3, function () env.awful.layout.inc(-1) end),
                           env.awful.button({ }, 2, function () env.awful.layout.set(env.awful.layout.layouts[1]) end),
                           env.awful.button({ }, 4, function () env.awful.layout.inc( 1) end),
                           env.awful.button({ }, 5, function () env.awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = env.awful.widget.taglist {
        screen  = s,
        filter  = env.awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = env.awful.widget.tasklist {
        screen  = s,
        filter  = env.awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = env.awful.wibar({ position = "bottom", screen = s, height = dpi(32) })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = env.wibox.layout.align.horizontal,
        { -- Left widgets
            layout = env.wibox.layout.fixed.horizontal,
            s.mylayoutbox,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = env.wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            env.wibox.widget.systray(),
            clock
        },
    }
end)
-- }}}
