local env = require("env")
local menubar = require("menubar")
require("awful.hotkeys_popup.keys")

-- {{{ Mouse bindings
root.buttons(env.gears.table.join(
    env.awful.button({ }, 3, function () mymainmenu:toggle() end)
))
-- }}}

-- {{{ Key bindings

local focus_by_dir = function(dir)
    return function ()
        env.awful.client.focus.global_bydirection(dir)
        -- make sure the new focused client is on top if clients are overlapping, e.g. floating clients
        if client.focus then client.focus:raise() end
    end
end

globalkeys = env.gears.table.join(
    -- Awesome functions
    env.awful.key(
        {modkey}, "Escape",
        function () mymainmenu:show() end,
        {description = "Show the Awesome WM menu", group = "awesome"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "Escape",
        function () env.awful.spawn.with_shell("sleep 1 && xset -display :0.0 dpms force off") end,
        {description = "Turn off the screen", group = "awesome"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "Escape",
        awesome.restart,
        {description = "Reload Awesome WM", group = "awesome"}
    ),
    env.awful.key(
        {modkey, "Control", "Mod1"}, "Escape",
        awesome.quit,
        {description = "Log out (Quit Awesome WM)", group = "awesome"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "/", -- question mark
        env.hotkeys_popup.show_help,
        {description="show help", group="awesome"}
    ),
    env.awful.key(
        {modkey}, "Print",
        function () env.awful.spawn.with_shell("import $(xdg-user-dir DOWNLOAD)/$(date +'%F_%H%M%S%z').png") end,
        {description = "Take a screenshot", group = "awesome"}
    ),

    -- Tag navigation
    env.awful.key(
        {modkey}, "u",
        env.awful.tag.viewprev,
        {description = "View previous tag", group = "tag"}
    ),
    env.awful.key(
        {modkey}, "o",
        env.awful.tag.viewnext,
        {description = "View next tag", group = "tag"}
    ),
    env.awful.key(
        {modkey}, "y",
        env.awful.tag.history.restore,
        {description = "View last viewed tag", group = "tag"}
    ),

    -- Client focus
    env.awful.key(
        {modkey}, "i",
        focus_by_dir("up"),
        {description = "Focus on client above focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "j",
        focus_by_dir("left"),
        {description = "Focus on client left of focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "k",
        focus_by_dir("down"),
        {description = "Focus on client below focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "l",
        focus_by_dir("right"),
        {description = "Focus on client right of focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "Up",
        focus_by_dir("up"),
        {description = "Focus on client above focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "Left",
        focus_by_dir("left"),
        {description = "Focus on client left of focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "Down",
        focus_by_dir("down"),
        {description = "Focus on client below focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "Right",
        focus_by_dir("right"),
        {description = "Focus on client right of focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, ";",
        env.awful.client.urgent.jumpto,
        {description = "Focus on urgent client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "h",
        function ()
            env.awful.client.focus.history.previous()
            if client.focus then client.focus:raise()end
        end,
        {description = "Focus on last focused client", group = "client"}
    ),

    -- Client de-minimization
    env.awful.key(
        {modkey, "Shift"}, "m",
        function ()
            local c = env.awful.client.restore()
            -- focus restored client
            if c then c:emit_signal("request::activate", "key.unminimize", {raise = true}) end
        end,
        {description = "Restore and focus minimized client", group = "client"}
    ),

    -- Screen focus
    env.awful.key(
        {modkey, "Control"}, "i",
        function () env.awful.screen.focus_bydirection("up") end,
        {description = "Focus on screen above focused screen", group = "client"}
    ),
    env.awful.key(
        {modkey, "Control"}, "j",
        function () env.awful.screen.focus_bydirection("left") end,
        {description = "Focus on screen left of focused screen", group = "client"}
    ),
    env.awful.key(
        {modkey, "Control"}, "k",
        function () env.awful.screen.focus_bydirection("down") end,
        {description = "Focus on screen below focused screen", group = "client"}
    ),
    env.awful.key(
        {modkey, "Control"}, "l",
        function () env.awful.screen.focus_bydirection("right") end,
        {description = "Focus on screen right of focused screen", group = "client"}
    ),
    env.awful.key(
        {modkey, "Control"}, "Up",
        function () env.awful.screen.focus_bydirection("up") end,
        {description = "Focus on screen above focused screen", group = "client"}
    ),
    env.awful.key(
        {modkey, "Control"}, "Left",
        function () env.awful.screen.focus_bydirection("left") end,
        {description = "Focus on screen left of focused screen", group = "client"}
    ),
    env.awful.key(
        {modkey, "Control"}, "Down",
        function () env.awful.screen.focus_bydirection("down") end,
        {description = "Focus on screen below focused screen", group = "client"}
    ),
    env.awful.key(
        {modkey, "Control"}, "Right",
        function () env.awful.screen.focus_bydirection("right") end,
        {description = "Focus on screen right of focused screen", group = "client"}
    ),

    -- Client position
    env.awful.key(
        {modkey, "Shift"}, "i",
        function () env.awful.client.swap.global_bydirection("up") end,
        {description = "Swap focused client with client above focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "j",
        function () env.awful.client.swap.global_bydirection("left") end,
        {description = "Swap focused client with client left of focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "k",
        function () env.awful.client.swap.global_bydirection("down") end,
        {description = "Swap focused client with client below focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "l",
        function () env.awful.client.swap.global_bydirection("right") end,
        {description = "Swap focused client with client right of focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "Up",
        function () env.awful.client.swap.global_bydirection("up") end,
        {description = "Swap focused client with client above focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "Left",
        function () env.awful.client.swap.global_bydirection("left") end,
        {description = "Swap focused client with client left of focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "Down",
        function () env.awful.client.swap.global_bydirection("down") end,
        {description = "Swap focused client with client below focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "Right",
        function () env.awful.client.swap.global_bydirection("right") end,
        {description = "Swap focused client with client right of focused client", group = "client"}
    ),

    -- Tiled layout structure
    env.awful.key(
        {modkey, "Control"}, "o",
        function () env.awful.tag.incmwfact( 0.05) end,
        {description = "Increase the width factor of \"master\" clients", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Control"}, "u",
        function () env.awful.tag.incmwfact(-0.05) end,
        {description = "Decrease the width factor of \"master\" clients", group = "layout"}
    ),

    env.awful.key(
        {modkey, "Mod1"}, "i",
        function () env.awful.tag.incnmaster( 1, nil, true) end,
        {description = "Increase the number of master clients", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "k",
        function () env.awful.tag.incnmaster(-1, nil, true) end,
        {description = "Decrease the number of master clients", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "Up",
        function () env.awful.tag.incnmaster( 1, nil, true) end,
        {description = "Increase the number of master clients", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "Down",
        function () env.awful.tag.incnmaster(-1, nil, true) end,
        {description = "Decrease the number of master clients", group = "layout"}
    ),

    env.awful.key(
        {modkey, "Mod1"}, "l",
        function () env.awful.tag.incncol( 1, nil, true) end,
        {description = "Increase the number of columns", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "j",
        function () env.awful.tag.incncol(-1, nil, true) end,
        {description = "Decrease the number of columns", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "Right",
        function () env.awful.tag.incncol( 1, nil, true) end,
        {description = "Increase the number of columns", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "Left",
        function () env.awful.tag.incncol(-1, nil, true) end,
        {description = "Decrease the number of columns", group = "layout"}
    ),

    -- Layout selection
    env.awful.key(
        {modkey}, "Tab",
        function () env.awful.layout.inc( 1) end,
        {description = "Select next layout mode", group = "layout"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "Tab",
        function () env.awful.layout.inc(-1) end,
        {description = "Select previous layout mode", group = "layout"}
    ),

    -- Launch common programs
    env.awful.key(
        {modkey}, "Return",
        function () env.awful.spawn(env.default_programs.terminal) end,
        {description = "Open a terminal window", group = "launcher"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "Return",
        function () env.awful.spawn(env.default_programs.file_manager) end,
        {description = "Open a file manager window", group = "launcher"}
    ),
    env.awful.key(
        {modkey, "Control"}, "Return",
        function () env.awful.spawn(env.default_programs.browser) end,
        {description = "Open a web browser window", group = "launcher"}
    ),
    env.awful.key(
        {modkey, "Control", "Shift"}, "Return",
        function () env.awful.spawn(env.default_programs.private_browser) end,
        {description = "Open a private web browser window", group = "launcher"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "Return",
        function () env.awful.spawn(env.default_programs.editor) end,
        {description = "Open an editor window", group = "launcher"}
    ),
    env.awful.key(
        {modkey, "Mod1"}, "space",
        function () env.awful.screen.focused().mypromptbox:run() end,
        {description = "Show run prompt", group = "launcher"}
    )
)

clientkeys = env.gears.table.join(
    -- Close client
    env.awful.key(
        {modkey}, "w",
        function (c) c:kill() end,
        {description = "Close focused client", group = "client"}
    ),

    -- Minimize client
    env.awful.key(
        {modkey}, "m",
        function (c) c.minimized = true end,
        {description = "Minimize focused client", group = "client"}
    ),

    -- Client view modes
    env.awful.key(
        {modkey}, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "Toggle fullscreen mode for focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "f",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end,
        {description = "Toggle maximized for focused client", group = "client"}
    ),
    env.awful.key(
        {modkey}, "g",
        env.awful.client.floating.toggle,
        {description = "Toggle floating mode for focused client", group = "client"}
    ),
    env.awful.key(
        {modkey, "Shift"}, "g",
        function (c) c.ontop = not c.ontop end,
        {description = "Toggle keep-on-top mode for focused client", group = "client"}
    )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = env.gears.table.join(globalkeys,
        -- View tag only.
        env.awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = env.awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        env.awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = env.awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         env.awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        env.awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        env.awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = env.gears.table.join(
    env.awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    env.awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        env.awful.mouse.client.move(c)
    end),
    env.awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        env.awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}
