return {
    awful = require("awful"),
    awesome_dir = "/home/" .. os.getenv("USER") .. "/.config/awesome/",
    beautiful = require("beautiful"),
    default_programs = {
        browser = "google-chrome",
        private_browser = "google-chrome --incognito",
        editor = "code -n" or "subl -n" or "atom -n",
        file_manager = "thunar",
        terminal = "lxterminal"
    },
    gears = require("gears"),
    hotkeys_popup = require("awful.hotkeys_popup"),
    wibox = require("wibox")
}
