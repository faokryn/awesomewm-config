local env = require("env")

require("awful.autofocus")

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    if not awesome.startup then env.awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        env.awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = env.gears.table.join(
        env.awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            env.awful.mouse.client.move(c)
        end),
        env.awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            env.awful.mouse.client.resize(c)
        end)
    )

    env.awful.titlebar(c) : setup {
        { -- Left
            env.awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = env.wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = env.awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = env.wibox.layout.flex.horizontal
        },
        { -- Right
            env.awful.titlebar.widget.floatingbutton (c),
            env.awful.titlebar.widget.maximizedbutton(c),
            env.awful.titlebar.widget.stickybutton   (c),
            env.awful.titlebar.widget.ontopbutton    (c),
            env.awful.titlebar.widget.closebutton    (c),
            layout = env.wibox.layout.fixed.horizontal()
        },
        layout = env.wibox.layout.align.horizontal
    }
end)

client.connect_signal("focus", function(c) c.border_color = env.beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = env.beautiful.border_normal end)
-- }}}
